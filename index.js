/*
Name: Damilola Olagundoye
Language: Javascript/NODEJS
Time Complexity: 
  wordSearch O(6^n): wordSearch calls itself 6 times making the time complexity 6^n
  gridSearch: O(r*c) or O(n^2) since row and column are the same size and the column is being iterated in the row iteration
    since wordSearch is being called in gridSearch  the total time complexity for gridSearch is O(6^n * n*2)
  alphabetSoup: The iteration through the words takes O(n), calling gridSearch makes the total time complexity O(6^n * n*2 * w)
                where w is the number of words in the words array
  TOTAL TIME COMPLEXITY: O(6^n * n*2 * w)
Space Complexity: 
  wordSearch: O(n)
  gridSearch: not includng the word variable O(n) for the creation of new SET
  alphabetSoup: O(n) for the creation of temp
*/

let puzzle1 = [
  ["H", "A", "S", "D", "F"],
  ["G", "E", "Y", "B", "H"],
  ["J", "K", "L", "Z", "X"],
  ["C", "V", "B", "L", "N"],
  //["G", "O", "O", "D", "O"]
  ["O", "D", "O", "O", "G"],
];

let word1 = ["HELLO", "GOOD", "BYE"];

let puzzle2 = [
  ["A", "B", "C"],
  ["D", "E", "F"],
  ["G", "H", "I"],
];

let word2 = ["ABC", "AEI"];

function alphabetSoup(grid, words) {
  words.forEach((word) => {
    let temp = gridSearch(grid, word).join(" ");
    console.log(temp);
  });
}

function gridSearch(grid, word) {
  let visited = new Set();
  let answer = null;

  //Taxes care of any spaces in the word
  word = word.split(" ").join("");

  for (let r = 0; r < grid.length; r++) {
    for (let c = 0; c < grid[0].length; c++) {
      wordSearch(r, c, grid, word, 0, visited);
    }
  }

  function wordSearch(r, c, grid, word, i, visited) {
    let ROW = grid.length - 1;
    let COL = grid[0].length - 1;

    let current = r + ":" + c;

    if (
      r < 0 ||
      c < 0 ||
      r > ROW ||
      c > COL ||
      visited.has(current) ||
      grid[r][c] !== word[i]
    )
      return;

    visited.add(current);

    if (grid[r][c] === word[word.length - 1]) {
      let temp = [...visited];
      answer = [word, temp[0], temp[temp.length - 1]];
      return;
    }

    wordSearch(r + 1, c, grid, word, i + 1, visited);
    wordSearch(r - 1, c, grid, word, i + 1, visited);
    wordSearch(r, c + 1, grid, word, i + 1, visited);
    wordSearch(r, c - 1, grid, word, i + 1, visited);
    //Diagonal
    wordSearch(r + 1, c + 1, grid, word, i + 1, visited);
    wordSearch(r - 1, c - 1, grid, word, i + 1, visited);

    visited.clear();
  }

  return answer;
}

alphabetSoup(puzzle1, word1);
console.log("----------------------------");
alphabetSoup(puzzle2, word2);
